using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AdherirAlJugador : MonoBehaviour
{
    public Transform jugadorTransform;
    public Vector3 offset = new Vector3(0f, 1f, 0f);

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            VincularObjetoAlJugador(other.transform);
        }
    }

    private void VincularObjetoAlJugador(Transform playerTransform)
    {
        GameObject angelWingsInstance = Instantiate(gameObject, playerTransform.position + offset, Quaternion.identity);
        angelWingsInstance.transform.parent = playerTransform; 
        gameObject.SetActive(false);

        

    }




}




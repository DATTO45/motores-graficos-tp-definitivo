using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{
    public Transform puntoBala;
    public GameObject balaPrefab;
    public float balaVelocidad = 10;
    public GameObject powerUp;


    private MeshRenderer meshRenderer;

    private bool armaActivada = false;
    private bool teclaQActivada = false;



    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.enabled = false;
    }

    void Update()
    {
        if (armaActivada && !teclaQActivada && Input.GetKeyDown(KeyCode.Q))
        {
            var bala = Instantiate(balaPrefab, puntoBala.position, puntoBala.rotation);
            bala.GetComponent<Rigidbody>().velocity = puntoBala.forward * balaVelocidad;

        }
    }

    public void DesactivarArma()
    {
        UnityEngine.Debug.Log("Arma desactivada");
        armaActivada = false;
        teclaQActivada = false;
        meshRenderer.enabled = false;



    }

    public void ActivarArma()
    {
        UnityEngine.Debug.Log("Arma activada");
        armaActivada = true;

        meshRenderer.enabled = true;


    }


    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PowerUp"))
        {
            teclaQActivada = true;
            ActivarArma();
        }
    }

}

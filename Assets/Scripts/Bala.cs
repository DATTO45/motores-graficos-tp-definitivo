using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    public float life = 3;

    void OnCollisionEnter(Collision collision)
    {


        if (collision.gameObject.CompareTag("PowerUp"))
        {
            ActivarBala();
        }
        else if (collision.gameObject.CompareTag("Enemigo"))
        {
            Enemigo4 enemigoScript = collision.gameObject.GetComponent<Enemigo4>();

           
            if (enemigoScript != null)
            {
                Destroy(enemigoScript.cadena.gameObject);
                Destroy(collision.gameObject);
            }

        }

        Destroy(gameObject);

    }

    public void ActivarBala()
    {

        gameObject.SetActive(true);
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraControl : MonoBehaviour
{
    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 2.0f;

    private float rotacionX = 0.0f;


    void Update()
    {

        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 movimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical) * velocidadMovimiento;
        transform.Translate(movimiento * Time.deltaTime);



        float mouseX = Input.GetAxis("Mouse X") * velocidadRotacion;
        float mouseY = Input.GetAxis("Mouse Y") * velocidadRotacion;
        rotacionX -= mouseY;
        rotacionX = Mathf.Clamp(rotacionX, -90.0f, 90.0f);


        transform.localRotation = Quaternion.Euler(rotacionX, 0.0f, 0.0f);
        transform.parent.Rotate(Vector3.up * mouseX);
    }

}

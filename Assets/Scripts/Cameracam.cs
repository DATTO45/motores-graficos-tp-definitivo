using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cameracam : MonoBehaviour
{
    public GameObject Player;
   
    private Vector3 posicionRelativa;

    public float velocidadRotacionXY;
    private float rotacionX = 0f;
    private float rotacionY = 0f;

    

    void Start()
    {
        posicionRelativa = transform.position - Player.transform.position;
    }

    void LateUpdate()
    {
        
            float rotacionMouseX = Input.GetAxis("Mouse X") * velocidadRotacionXY;
            float rotacionMouseY = Input.GetAxis("Mouse Y") * velocidadRotacionXY;

            rotacionX -= rotacionMouseY;
            rotacionY += rotacionMouseX;

            rotacionX = Mathf.Clamp(rotacionX, -90f, 90f);

            Player.transform.rotation = Quaternion.Euler(0f, rotacionY, 0f);
            transform.localRotation = Quaternion.Euler(rotacionX, 0f, 0f);

            transform.position = Player.transform.position + posicionRelativa;
        
       

      
    
    }


}



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesaparecerObjeto : MonoBehaviour
{
    public Arma arma;
    public Bala[] balas;
    public EnemigoCorredor enemigoCorredor;

    private bool corredorActivado = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(!corredorActivado) 
            { 
            if (enemigoCorredor != null) 
                {
                    enemigoCorredor.ReanudarCorredor();
                    corredorActivado = true;
                }
            }

            if (arma != null)
            {
               DesactivarObjeto(arma.gameObject);
              
            }
            if (balas != null)
            {
                foreach (Bala bala in balas)
                {
                    DesactivarObjeto(bala.gameObject);
                   
                }
            }
            Destroy(gameObject);

        }
    }

    private void DesactivarObjeto(GameObject obj)
    {
        MeshRenderer meshRenderer = obj.GetComponent<MeshRenderer>();
        if (meshRenderer != null) 
        {
            meshRenderer.enabled = false;
        }
        obj.SetActive(false);
    }
}

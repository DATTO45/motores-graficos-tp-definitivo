using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistroyPowerUp : MonoBehaviour
{
    public Arma arma;
    public Bala[] balas;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Arma arma = other.GetComponent<Arma>();
            if (arma != null)
            {
                arma.ActivarArma();

                arma.DesactivarArma();
            }
            if (balas != null)
            {
                foreach (Bala bala in balas)
                {
                    bala.ActivarBala();
                }
            }
            Destroy(gameObject);
        }
    }

}

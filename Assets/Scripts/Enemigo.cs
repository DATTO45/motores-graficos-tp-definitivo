using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public float velocidad = 11.0f;
    public float distancia = 4.43f;
    public float limiteIzquierda = -14.55f;
    public float limiteDerecha = 59.9f;
    private bool moverDerecha = true;

    private Vector3 puntoInicial;
    private float posicionY;

    private Player jugador;
    private Vector3 initialPosition;
    void Start()
    {

        puntoInicial = transform.position;
        posicionY = transform.position.y;
        jugador = FindObjectOfType<Player>();
        initialPosition = transform.position;
    }

    void Update()
    {
        if (moverDerecha)
        {

            transform.Translate(Vector3.right * velocidad * Time.deltaTime);
        }
        else
        {

            transform.Translate(Vector3.left * velocidad * Time.deltaTime);
        }


        Vector3 nuevaPosicion = transform.position;
        nuevaPosicion.y = posicionY;
        transform.position = nuevaPosicion;

        if (transform.position.x >= limiteDerecha)
        {

            moverDerecha = false;
        }
        else if (transform.position.x <= limiteIzquierda)
        {

            moverDerecha = true;
        }

    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            UnityEngine.Debug.Log("Colision con el jugador detectada en enemigos.");
            jugador.ResetToInitialPosition();

        }
    }

    public void ResetToInitialPosition()
    {
        transform.position = initialPosition;
    }

}

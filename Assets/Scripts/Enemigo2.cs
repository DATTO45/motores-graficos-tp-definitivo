using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo2 : MonoBehaviour
{
    public float velocidad = 2.0f;
    public float puntoInicio = 145.15f;
    public float puntoFinal = 162.43f;

    private bool moviendoseHacialaDerecha = true;
    private Vector3 posicionInicial;

    void Start()
    {
        posicionInicial = transform.position;
    }

    void Update()
    {
        if (moviendoseHacialaDerecha)
        {
            transform.Translate(Vector3.right * velocidad * Time.deltaTime);

            if (transform.position.x >= puntoFinal)
            {
                moviendoseHacialaDerecha = false;
            }

        }
        else
        {
            transform.Translate(Vector3.left * velocidad * Time.deltaTime);
            if (transform.position.x <= puntoInicio)
            {
                moviendoseHacialaDerecha = true;
            }

        }
    }
}
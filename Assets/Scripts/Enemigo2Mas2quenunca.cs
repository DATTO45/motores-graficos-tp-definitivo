using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo2Mas2quenunca : MonoBehaviour
{
    public float velocidad = 7.0f;
    public float puntoInicio = -28f;
    public float puntoFinal = -10f;

    private bool moviendoseHacialaDerecha = false;
    private Vector3 posicionInicial;

    void Start()
    {
        posicionInicial = transform.position;
    }

    void Update()
    {
        if (moviendoseHacialaDerecha)
        {
            transform.Translate(Vector3.right * velocidad * Time.deltaTime);

            if (transform.position.x >= puntoFinal)
            {
                moviendoseHacialaDerecha = false;
            }

        }
        else
        {
            transform.Translate(Vector3.left * velocidad * Time.deltaTime);
            if (transform.position.x <= puntoInicio)
            {
                moviendoseHacialaDerecha = true;
            }
        }





    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo3 : MonoBehaviour
{
    public float topPosition = 9f;
    public float bottomPosition = -50f;
    public float speed = 5.0f;

    private bool movingUp = true;


    void Update()
    {
        if (movingUp)
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);

            if (transform.position.y >= topPosition)
            {
                movingUp = false;
            }
        }
        else
        {
            transform.Translate(Vector3.down * speed * Time.deltaTime);

            if (transform.position.y <= bottomPosition)
            {
                movingUp = true;
            }
        }
    }

}

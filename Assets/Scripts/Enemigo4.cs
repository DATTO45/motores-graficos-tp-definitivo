using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo4 : MonoBehaviour
{
    public Transform jugador;
    public float velocidad = 5f;

   
    public Transform cadena;

    private Vector3 offsetCadena;

    void Start() 
    {
        if (cadena == null)
        {
            UnityEngine.Debug.LogError("Objeto Cadena no asignado en el Inspector.");
            return;
        }


        offsetCadena = cadena.position - transform.position;
        cadena.parent = null;

        cadena.position = transform.position + offsetCadena;
    }



    void Update()
    {
        if (Vector3.Distance(transform.position, jugador.position) < 13f)
        {
            Vector3 direccion = jugador.position - transform.position;
            direccion.Normalize();

            transform.Translate(direccion * velocidad * Time.deltaTime);

        }
        
        GameObject[] enemigos = GameObject.FindGameObjectsWithTag("Enemigo");

        
        if (enemigos.Length == 0)
        {
            Destroy(gameObject); 
            Destroy(cadena.gameObject);
        }



    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemigoCorredor : MonoBehaviour
{
    public float velocidad = 0f;

    void Update()
    {
        transform.Translate(Vector3.forward * velocidad * Time.deltaTime);

        Collider[] colliders = Physics.OverlapSphere(transform.position, 0.5f);
        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("DesactivarCorredor"))
            {
                DetenerCorredor();
                ReiniciarJuego(); 
                break;
            }
        }
    }

    void DetenerCorredor()
    {
        velocidad = 0f;
        UnityEngine.Debug.Log(" ¡Perdiste!");
    }

    void ReiniciarJuego()
    {
        
        SceneManager.LoadScene("Juegaso");
    }

    public void ReanudarCorredor()
    {
        velocidad = 4f;
    }



}

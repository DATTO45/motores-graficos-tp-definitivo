using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoTubo : MonoBehaviour
{
    public GameObject enemigoTuberia;
    private bool enBucle = false;

    void Start()
    {
        
        InvokeRepeating("AlternarEstadoEnemigo", 0f, 4f);
    }

    void AlternarEstadoEnemigo()
    {
        enBucle = !enBucle;

        if (enBucle)
        {
            
            enemigoTuberia.SetActive(true);
            Invoke("DesactivarEnemigo", 2f);
        }
        else
        {
            
            enemigoTuberia.SetActive(false);
            Invoke("ActivarEnemigo", 2f);
        }
    }

    void DesactivarEnemigo()
    {
        enemigoTuberia.SetActive(false);
    }

    void ActivarEnemigo()
    {
        enemigoTuberia.SetActive(true);
    }


}

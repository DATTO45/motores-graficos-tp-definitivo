using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enter : MonoBehaviour
{
    public GameObject menu;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (menu != null)
            {
                menu.SetActive(false);
            }
        }
    }

}

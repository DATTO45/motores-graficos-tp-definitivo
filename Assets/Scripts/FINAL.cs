using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class FINAL : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            ReiniciarJuego();
        }
        else if (collision.collider.CompareTag("Finish"))
        {
            ReiniciarJuego();
            MostrarMenu();
        }
    }
    void ReiniciarJuego()
    {
        Scene escenaActual = SceneManager.GetActiveScene();
        SceneManager.LoadScene(escenaActual.name);
    }
    void MostrarMenu()
    {
        SceneManager.LoadScene("Juegaso");
    }

}

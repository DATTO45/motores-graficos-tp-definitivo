using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamerespawn : MonoBehaviour
{
    public float threshold;
    public Vector3 startPoint = new Vector3(-0.421f, 7.695f, -4.54f);

    void FixedUpdate()
    {
        if (transform.position.y < threshold)
        {
            transform.position = startPoint;
        }
    }



}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ganaste : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameObject corredor = GameObject.FindGameObjectWithTag("Corredor");

            if (corredor != null)
            {
                corredor.SetActive(false);
            }

            Destroy(gameObject); 
        }
    }


}

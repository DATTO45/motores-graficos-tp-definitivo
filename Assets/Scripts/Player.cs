using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;

using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocidad = 5.0f;
    public float gravedad = 9.81f;
    private CharacterController characterController;
    private Vector3 velocidadVertical;
    private Vector3 posicionInicial;

    public float fuerzaSalto = 7.0f;
    private int saltoRestantes = 2;

    public float minY = 36.48f;

    public Arma arma;
    private bool powerUpRecogido = false;


    private AudioSource SonidoDeSalto;

    

    private MeshRenderer playerMeshRenderer;

    public GameObject angelWings;
    private bool wingsRecogidas = false;
    private bool saltosInfinitosActivados = false;



    void Start()
    {
        characterController = GetComponent<CharacterController>();
        posicionInicial = transform.position;

        gameObject.tag = "Player";

        SonidoDeSalto = GetComponent<AudioSource>();

      

        playerMeshRenderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {

        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        Vector3 direccionCamara = Camera.main.transform.forward;

        Vector3 movimiento = direccionCamara * movimientoVertical + Camera.main.transform.right * movimientoHorizontal;
        movimiento.y = 0;

        movimiento *= velocidad;
        characterController.Move(movimiento * Time.deltaTime);

        if (characterController.isGrounded)
        {
            velocidadVertical.y = 0;
            saltoRestantes = 2;
        }
        else
        {
            velocidadVertical.y -= gravedad * Time.deltaTime;
        }



        if (Input.GetButtonDown("Jump") && (saltoRestantes > 0 || saltosInfinitosActivados))
        {
            velocidadVertical.y = saltosInfinitosActivados ? fuerzaSalto : fuerzaSalto * 0.5f; 
            if (!saltosInfinitosActivados)
            {
                saltoRestantes--;
            }
            SonidoDeSalto.Play();
        }


        if (transform.position.y < minY)
        {
            ResetToInitialPosition();
        }







        characterController.Move(velocidadVertical * Time.deltaTime);

        if (powerUpRecogido && arma != null)
        {
            arma.ActivarArma();

        }



        if (wingsRecogidas && angelWings != null)
        {
            
            angelWings.transform.position = transform.position;
            angelWings.transform.rotation = transform.rotation;
        }




    }



    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PowerUp"))
        {

            Destroy(other.gameObject);
            powerUpRecogido = true;
        }

        else if (other.CompareTag("AngelWings"))
        {
            wingsRecogidas = true;
            saltosInfinitosActivados = true;
        }

        else if (other.CompareTag("desaparecerArma"))
        {
            DesactivarArmaYBala();
        }





    }






    
    private void DesactivarArmaYBala() 
    { 
    if (arma != null) 
        {
           MeshRenderer meshRendererArma = arma.GetComponent<MeshRenderer>();
            if (meshRendererArma != null) 
            {
                meshRendererArma.enabled = false;
            }
            arma.gameObject.SetActive(false);
        }
       
        if (arma != null && arma.balaPrefab != null)
        {
            GameObject balaPrefab = arma.balaPrefab;
            balaPrefab.SetActive(false);
        }
    }

    


    public void ResetToInitialPosition()
    {
        UnityEngine.Debug.Log("ColisiónReseteando a la posición inicial en Antonio.");
        transform.position = posicionInicial;

    }

}

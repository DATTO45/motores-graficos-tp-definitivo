using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public GameObject puerta;  
    public string puntoNombre = "Punto";  
    private int puntosRestantes;

    private void Start()
    {
        
        puntosRestantes = GameObject.FindGameObjectsWithTag(puntoNombre).Length;
    }

    
    public void PuntoDestruido()
    {
        
        puntosRestantes--;

       
        if (puntosRestantes <= 0)
        {
            
            Destroy(puerta);
        }
    }

}

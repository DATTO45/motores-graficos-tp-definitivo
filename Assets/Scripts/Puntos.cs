using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puntos : MonoBehaviour
{
    private Puerta Puerta;

    private void Start()
    {
        
      Puerta = GameObject.FindObjectOfType<Puerta>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            gameObject.SetActive(false);

          
            Puerta.PuntoDestruido();
        }
    }


}

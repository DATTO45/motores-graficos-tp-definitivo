using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suelogiratorio : MonoBehaviour
{
    public float velocidadRotacion = 30.0f;



    private void Update()
    {
        transform.Rotate(Vector3.right, velocidadRotacion * Time.deltaTime);
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tiempo : MonoBehaviour
{
    private bool juegoPausado = true;
    public GameObject panelPausa;
    public Camera mainCamera;
    private AudioListener audioListener;
    
    void Start()
    {
        Time.timeScale = 0f;
        panelPausa.SetActive(true);
        audioListener = mainCamera.GetComponent<AudioListener>();
        audioListener.enabled = !juegoPausado;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {

            if (juegoPausado)
            {
                DespausarJuego();
            }
            else
            {
                PausarJuego();
            }

        }




    }

    void PausarJuego()
    {
        Time.timeScale = 0f;
        juegoPausado = true;
        panelPausa.SetActive(false);
        audioListener.enabled = false;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;

    }
    void DespausarJuego()
    {
        Time.timeScale = 1f;
        juegoPausado = false;
        panelPausa.SetActive(false);
        audioListener.enabled = true;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

}
